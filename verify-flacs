#!/usr/bin/env bash

TARGET="$1"
PROM_DIR="$2"

FLAC_TOOL="flac"
ARGS="--totally-silent -wt"

LOGFILE=/tmp/verify-flacs.log
CORRUPT_FILES=0
SCANNED_FILES=0

PREFIX="flacverifier"

set -u

prepare_logfile() {
    if [ ! -e ${LOGFILE} ]; then
        set -e
        touch ${LOGFILE}
        set +e
    fi
    cat /dev/null > ${LOGFILE}
    chmod 600 ${LOGFILE}
}

print_metrics() {
    echo "# TYPE ${PREFIX}_scanned_files_total gauge"
    echo "${PREFIX}_scanned_files_total ${SCANNED_FILES}"
    
    echo "# TYPE ${PREFIX}_corrupt_files_total gauge"
    echo "${PREFIX}_corrupt_files_total ${CORRUPT_FILES}"

    echo "# TYPE ${PREFIX}_timestamp_seconds gauge"
    echo "${PREFIX}_timestamp_seconds ${RUNTIME}"
    
    echo "# TYPE ${PREFIX}_heartbeat_seconds gauge"
    echo "${PREFIX}_heartbeat_seconds ${START_TIME}"
}

verify() {
    # Check whether the needed 'flac' binary is available..
    if [ -z $(command -v "$FLAC_TOOL") ]; then
        echo "No command '"$FLAC_TOOL"' found, sorry. Please install it first. "
        exit 1
    fi

    # Check if the user supplied the target directory to scan..
    if [ -z "$1" ]; then
        echo "Specify which directory to scan"
        exit 1
    fi

    # Check if the supplied dir exists
    if [ ! -d "$1" ]; then
        echo "Specified dir does not exist"
        exit 1
    fi
}

verify_files() {
    START_TIME=$(date +%s)
    while read file; do
        $FLAC_TOOL $ARGS "$file";
        if [[ 0 -ne "$?" ]]; then
            echo "$file" >> ${LOGFILE}
            CORRUPT_FILES=$(expr ${CORRUPT_FILES} + 1)
        fi;
        SCANNED_FILES=$(expr ${SCANNED_FILES} + 1)
    done <<< $(find "$1" -iname *.flac)
    RUNTIME=$(expr $(date +%s) - ${START_TIME})
}

verify "$1"
prepare_logfile
verify_files "$1"

if [ ! -z "${PROM_DIR}" ]; then
    print_metrics | sponge "${PROM_DIR}/flacs.prom"
elif [ $CORRUPT_FILES -ne 0 ]; then
    exit 1
fi
